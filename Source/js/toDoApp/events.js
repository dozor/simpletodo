import createElem from './create-elem'
import showList from './show-list'

export default function EventBind (){
    document.body.onclick = function (e) {
        let target = e.target;
        let targetRole = target.getAttribute("data-role");
        switch (targetRole) {
            case "create-elem":
                let select = document.querySelector(".js-select");
                let type = select.value;
                createElem(type);
                break;
            case "show-list":
                showList();
                break;
            default:
                e.stopPropagation();
                e.preventDefault();
                break;
        }
    }
}