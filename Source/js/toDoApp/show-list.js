import {storage} from "./storage"

export default function showList() {
    let elemsFinalMarkup = '';
    let rootElem = document.querySelector('.js-root');
    storage.forEach(prepareToDoTree);
    function prepareToDoTree (item){
        let elemMarkup = '', elemsMarkup = '';
        item.values.forEach(
            function (elem) {
                elemMarkup += `
                    <li class="value-list__elem">
                        <div class="value-list__elem-title">${elem.title}</div>
                        <div class="value-list__elem-value">${elem.value}</div>
                    </li>
                `
                return elemMarkup;
            }
        );
        elemsFinalMarkup += `
            <li class="result-list__elem">
                <div class="result-list__elem-title">${item.type}</div>
                <ul class="value-list">${elemMarkup}</ul>
            </li>
        `;
    }
    rootElem.innerHTML = `<ul class="result-list">${elemsFinalMarkup}</ul>`;
    rootElem.setAttribute("data-state", "show-list");
}