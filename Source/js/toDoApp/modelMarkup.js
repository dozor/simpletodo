import {id} from "./create-elem";

export let markup = '';

export default function makeMarkupModel (type){
    switch (type) {
        case "contacts":
            markup = `<div id=${id} class="to-do-unit" data-type="contacts">
                <label class="avatar-block">
                    <img class="avatar-block__image" src="http://placehold.it/50x50">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Name</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="name" type="text">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Surname</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="surname" type="text">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Phone</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="phone" type="text">
                </label>
            </div>`;
            break;

        case "persons":
            markup = `<div id=${id} class="to-do-unit" data-type="persons">
                <label class="avatar-block">
                    <img class="avatar-block__image" src="http://placehold.it/50x50">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Name</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="name" type="text">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Surname</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="surname" type="text">
                </label>
            </div>`;
            break;

        case "deals":
            markup = `<div id=${id} class="to-do-unit" data-type="deals">
                <label class="avatar-block">
                    <img class="avatar-block__image" src="http://placehold.it/50x50">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Title</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="title" type="text">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Description</div>
                    <textarea class="data-block__value-place" data-role="data-storage" data-title="description"></textarea>
                </label>
                <label class="data-block">
                    <div class="data-block__title">Company</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="company" type="text">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Email</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="email" type="email">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Phone</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="phone" type="phone">
                </label>
            </div>`;
            break;

        case "events":
            markup = `<div id=${id} class="to-do-unit" data-type="events">
                <label class="avatar-block">
                    <img class="avatar-block__image" src="http://placehold.it/50x50">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Title</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="title" type="text">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Time</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="time" type="text">
                </label>
                <label class="data-block">
                    <div class="data-block__title">Address</div>
                    <input class="data-block__value-place" data-role="data-storage" data-title="address" type="text">
                </label>
            </div>`;
            break;
        default:
            markup = `Something went wrong`;
            break;
    }
    return markup;
}
//console.log(markup);
