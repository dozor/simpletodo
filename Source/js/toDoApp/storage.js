import {id} from "./create-elem";

export let storage = [];

export default function saveData(toDoUnit, toDoUnitType) {
    let storageElem = {};
    let storageElemValues = [];
    let arrStorageDataElem = toDoUnit.querySelectorAll('[data-role="data-storage"]');
    arrStorageDataElem.forEach(function(element, index, array){
        let value = element.value;
        let title = element.getAttribute("data-title");
        storageElem = {
            type : toDoUnitType,
            values: storageElemValues
        };
        storageElemValues[index] = {
            title: title,
            value: value
        };
        storage[id] = storageElem;
    });
}

