import makeMarkupModel, {markup} from "./modelMarkup"
import saveData from "./storage"

export let id = 0;

export default function createElem (elemType){
    let rootElem = document.querySelector('.js-root');
    let state = rootElem.getAttribute("data-state");

    makeMarkupModel(elemType);
    function pasteNewElem (markup){
        document.querySelector('.js-root').innerHTML = markup;
    }
    if (state === "init" || state === "show-list"){
        rootElem.setAttribute("data-state", "work");
        pasteNewElem(markup);
    } else {
        let toDoUnit = document.querySelector('.to-do-unit');
        let toDoUnitType = toDoUnit.getAttribute("data-type");
        // validateData();
        saveData(toDoUnit, toDoUnitType);
        pasteNewElem(markup);
        id++;
    };

}