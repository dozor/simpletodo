var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var path = require('path');
var uglify = require('gulp-uglify');
var stylus = require('gulp-stylus');

var paths = {
    TODO: {
        src: {
            js: './Source/js/app.js',
            allJs: './Source/js/**/*.js',
            style: './Source/styles/index.styl',
            allStyle: './Source/styles/**/*.styl'
        },
        build: {
            js: './Build/js/',
            css: './Build/Styles/'
        }
    }
};

function compile() {
    var bundler = browserify(paths.TODO.src.js, { debug: true }).transform(babel);
    function rebundle() {
        bundler.bundle()
            .on('error', function(err) { console.error(err); this.emit('end'); })
            .pipe(source('scripts.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('./Build/js'));
    }
    rebundle();
    return true;
}

function compileProd() {
    var bundler = browserify(paths.TODO.src.js, { debug: false }).transform(babel);
    function rebundle() {
        bundler.bundle()
            .on('error', function(err) { console.error(err); this.emit('end'); })
            .pipe(source('scripts.js'))
            .pipe(buffer())
            .pipe(uglify())
            .pipe(gulp.dest('./Build/js'));
    }
    rebundle();
    return true;
}

gulp.task('TODO-style', function () {
        gulp.src(paths.TODO.src.style)
        .pipe(sourcemaps.init())
        .pipe(stylus())
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: true
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.TODO.build.css));
});
gulp.task('TODO-style-prod', function () {
    gulp.src(paths.TODO.src.style)
        .pipe(stylus({
            compress: true
        }))
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(paths.TODO.build.css));
});
gulp.task('TODO-js', function () { return compile();});
gulp.task('TODO-js-prod', function () { return compileProd();});

gulp.task('TODO-serve', ['TODO-style', 'TODO-js'], function() {
    browserSync({
        notify: false,
        open: true,
        server: {
            baseDir: "./Build"
        }
    });
    gulp.watch([paths.TODO.src.allStyle], ['TODO-style', browserSync.reload]);
    gulp.watch([paths.TODO.src.allJs], ['TODO-js', browserSync.reload]);
});

gulp.task('TODO-build-prod', ['TODO-style-prod', 'TODO-js-prod'], function () {});
